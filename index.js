const alasql = require( "alasql");

/**
 * Create table.
 */
alasql(`CREATE TABLE MYTABLE (
    id varchar(50) NOT NULL,
    text varchar(10) NOT NULL,
    PRIMARY KEY (id)
  )`);

/**
 * Insert sample data (2 rows).
 */
const rowId1 = 'id#1';
alasql('insert into MYTABLE(id, text) values (?, ?)', [
    rowId1,
    'first text',
  ]);
const rowId2 = 'id#2';
alasql('insert into MYTABLE(id, text) values (?, ?)', [
    rowId2,
    'second text',
]);
    
/** 
 * Check that the rows are correctly inserted
 */
const rows = alasql("select * from MYTABLE");
console.log("rows:", rows)
if (rows.length !== 2) {
    throw new Error(`Wrong row number, the table should contain two rows`)
}

/**
 * Simple query that returns :
 * - two rows in alasql 4.1.11
 * - zero row in alasql 4.2.0 !
 */
const selectedByIdRows = alasql(`select entity.id, entity.text from MYTABLE as entity where entity.id in (?, ?)`, [
    rowId1,
    rowId2
]);
console.log("selectedByIdRows:", selectedByIdRows)
if (selectedByIdRows.length !== 2) {
    throw new Error(`Unexpected query result, got ${selectedByIdRows.length} rows instead of ${rows.length}`)
} else {
    console.log("Successful !")
}